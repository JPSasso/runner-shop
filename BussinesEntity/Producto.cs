﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesEntity
{
    public class Producto
    {
        private string _descripcion;
        public string descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        private string _imagen;
        public string imagen
        {
            get
            { return _imagen; }
            set
            { _imagen = value; }
        }

        private float precio;

        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        private string cod;

        public string Codigo
        {
            get { return cod; }
            set { cod = value; }
        }


        public override string ToString()
        {
            return cod + ": " + _descripcion;
        }

    }
}
