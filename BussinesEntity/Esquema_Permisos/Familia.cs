﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesEntity
{
    public class Familia : Permiso
    {
        private readonly List<Permiso> permisos = new List<Permiso>();
        public Familia(int id, string nombre, string desc) : base(id, nombre, desc) { }

        public override void Add(Permiso C)
        {
            permisos.Add(C);
        }

        public override object Display()
        {
            return permisos;
        }

        public override void Remove(Permiso C)
        {
            permisos.Remove(C);
        }
    }
}
