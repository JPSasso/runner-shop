﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesEntity
{
    public class Patente : Permiso
    {
        public Patente(int id, string nombre, string desc) : base(id, nombre, desc) { }

        public override void Add(Permiso P)
        {
            Console.WriteLine("Error, componente hoja");
        }

        public override object Display()
        {
            return this;
        }

        public override void Remove(Permiso P)
        {
            Console.WriteLine("Error, componente hoja");
        }
    }
}
