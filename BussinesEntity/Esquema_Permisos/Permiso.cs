﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesEntity
{
    public abstract class Permiso
    {
        private int id;
        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string desc;

        public string Descripcion
        {
            get { return desc; }
            set { desc = value; }
        }

        public Permiso(int id, string nombre, string desc)
        {
            this.desc = desc;
            this.nombre = nombre;
            this.id = id;
        }
        public abstract void Add(Permiso P);
        public abstract void Remove(Permiso P);
        public abstract object Display();
    }
}
