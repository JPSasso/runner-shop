﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BussinesEntity
{
    public class Rol : Permiso
    {
        public Rol(int id, string nombre, string desc) : base(id, nombre, desc) { }

        private readonly List<Permiso> Familias = new List<Permiso>();
        private readonly List<Permiso> Permisos = new List<Permiso>();

        public void AddFamilia(Permiso P)
        {
            Familias.Add(P);
        }
        public void AddPatente(Permiso P)
        {
            Permisos.Add(P);
        }
        public object DisplayFamilias()
        {
            return Familias;
        }
        public object DisplayPermissions()
        {
            return Permisos;
        }
        public override void Remove(Permiso P)
        {
            if (P.GetType() is Familia)
            {
                RemoveFamilia(P);
            }
            else if (P.GetType() is Patente)
            {
                RemovePatente(P);
            }
            else
            {
                Console.WriteLine(P.ToString() + " no se reconoce como un elemento de esquema de permisos");
            }
        }
        public void RemoveFamilia(Permiso P)
        {
            Familias.Remove(P);
        }
        public void RemovePatente(Permiso P)
        {
            Permisos.Remove(P);
        }
        public override void Add(Permiso P)
        {
            if (P.GetType() is Familia)
            {
                AddFamilia(P);
            }
            else if (P.GetType() is Patente)
            {
                AddPatente(P);
            }
            else
            {
                Console.WriteLine(P.ToString() + " no se reconoce como un elemento de esquema de permisos");
            }
        }

        public override object Display()
        {
            List<Permiso> TempPermisos = new List<Permiso>();
            foreach (Permiso G in this.Familias)
            {
                TempPermisos.AddRange((List<Permiso>)G.Display());
            }
            foreach (Permiso P in this.Permisos)
            {
                TempPermisos.Add((Permiso)P.Display());
            }
            var permisos = TempPermisos.GroupBy(x => x.ID)
                .Select(g => g.First())
                .OrderBy(x => x.ID)
                .ToList();
            return permisos;
        }
        public override string ToString()
        {
            return this.Nombre;
        }
    }
}
