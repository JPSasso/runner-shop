﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesEntity
{
    public class Usuario
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string username;

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        private string pssword;

        public string PassWord
        {
            get { return pssword; }
            set { pssword = value; }
        }
        private string mail;

        public string EMail
        {
            get { return mail; }
            set { mail = value; }
        }

        private Rol rol;

        public Rol PermisoDeUsuario
        {
            get { return rol; }
            set { rol = value; }
        }
        private bool bloqueado;

        public bool Bloqueado
        {
            get { return bloqueado; }
            set { bloqueado = value; }
        }

        public override string ToString()
        {
            return ID.ToString() + "_" + Nombre + "_" + Apellido + "_" +
                Username + "_" + PassWord + "_" + EMail + "_" + rol.ID.ToString();
        }
    }
}
