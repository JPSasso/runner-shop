﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinesEntity;
using DAL;

namespace BLL
{
    public class ProductoBLL
    {
        ProductoDAL DAL = new ProductoDAL();
        public List<Producto> GetProductos()
        {
            return DAL.GetProductos();
        }

        public int altaProducto(Producto P)
        {
            return DAL.AltaProducto(P);
        }

        public Producto GetProducto(string productCode)
        {
            return DAL.GetProducto(productCode);
        }
    }
}
