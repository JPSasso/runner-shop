﻿using BussinesEntity;
using DAL;
using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserBLL
    {
        private readonly UserDAL DAL = new UserDAL();
        DataIntegrityManager DIM = new DataIntegrityManager();

        public int LogIn(string usuario, string pw)
        {
            Usuario u = DAL.GetUsuario(usuario);
            if (u.Username != usuario)
            {
                return -3; // usuario invalido
            }
            CryptoManager CM = new CryptoManager();
            if (CM.Compare(u.PassWord, pw))
            {
                SessionManager SM = SessionManager.GetSession();
                SM.SetUsuarios(GetUsuarios());
                if (SM.GetUsuario(u.ID) != null)
                {
                    return 1; // sesion ok
                }
                else return -1; // sesion ya activa
            }
            else
            {
                return -2; // contraseña invalida
            }
        }
        public Usuario GetUsuario(string username)
        {
            return DAL.GetUsuario(username);
        }

        public int altaUsuario(Usuario user)
        {
            user.PassWord = CM.ApplyHash(user.PassWord);
            return DAL.AltaUsuario(user, new DataIntegrityManager().CalcularDVH(user));
        }

        public List<Usuario> GetUsuarios()
        {
            List<Usuario> usuarios = DAL.GetUsuarios();
            foreach (Usuario U in usuarios)
            {
                U.PermisoDeUsuario = DAL.GetUserRole(U);
            }
            return usuarios;
        }

        public int LogOut()
        {
            SessionManager SM = SessionManager.GetSession();
            SM.CloseSession();
            return 0;
        }
        public int BloquearUsuario(string username)
        {
            return DAL.BloquearUsuario(username);
        }
        CryptoManager CM = new CryptoManager();
        public int ModifUsuario(Usuario u)
        {
            return new UserDAL().ModifUsuario(u, new DataIntegrityManager().CalcularDVH(u));
        }
    }
}
