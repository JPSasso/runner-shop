﻿using BussinesEntity;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class RolesBLL
    {
        private readonly RolesDAL rolesDAL = new RolesDAL();
        public List<Permiso> GetRoles()
        {
            return rolesDAL.GetRoles();
        }

        public List<Permiso> GetPermisos()
        {
            return rolesDAL.GetPermisos();
        }

        public int AltaPermiso(Permiso p)
        {
            return rolesDAL.AltaPermiso(p);
        }

        public int BorrarPermiso(Permiso p)
        {
            return rolesDAL.BorrarPermiso(p);
        }

        public object GetGrupos()
        {
            return rolesDAL.getGrupos();
        }

        public int AddGrupo(Familia F)
        {
            return rolesDAL.AltaGrupo(F);
        }

        public int LinkearPermisoAGrupo(int idPermiso, int idGrupo)
        {
            return rolesDAL.LinkearPermisoAGrupo(idPermiso, idGrupo);
        }

        public List<Permiso> GetPermisosDeGrupo(int IdGrupo)
        {
            return rolesDAL.GetPermisosDeGrupo(IdGrupo);
        }

        public int ModifGrupo(Familia F)
        {
            return rolesDAL.ModifGrupo(F);
        }

        public Rol GetRol(int id)
        {
            return rolesDAL.GetRole(id);
        }
        public Rol GetRol(string id)
        {
            return rolesDAL.GetRole(id);
        }

        public int BorrarGrupo(Familia F)
        {
            return rolesDAL.BorrarGrupo(F);
        }

        public int AddCargo(Rol R)
        {
            return rolesDAL.AddCargo(R);
        }

        public int ModifCargo(Rol R)
        {
            return rolesDAL.ModifCargo(R);
        }

        public int BorrarCargo(Rol R)
        {
            return rolesDAL.EraseCargo(R);
        }

        public int DeslinkearPermisoAGrupo(int idPermiso, int idGrupo)
        {
            return rolesDAL.DeslinkearPermisoAGrupo(idPermiso, idGrupo);
        }
        public int DeslinkearPermisoACargo(int idPermiso, int idGrupo)
        {
            return rolesDAL.DeslinkearPermisoACargo(idPermiso, idGrupo);
        }
        public int LinkearGrupoACargo(int idgrupo, int idcargo)
        {
            return rolesDAL.LinkearGrupoACargo(idgrupo, idcargo);
        }

        public List<Permiso> GetGrupoDeCargo(int idcargo)
        {
            return rolesDAL.GetRolesFromCargo(idcargo);
        }

        public int LinkearPermisoACargo(int idperm, int idcargo)
        {
            return rolesDAL.LinkearPermisoACargo(idperm, idcargo);
        }

        public List<Permiso> GetPermisosDeCargo(int iD)
        {
            return rolesDAL.GetPermisosFromCargo(iD);
        }

        public int DeslinkearGrupoDeCargo(int idgrupo, int idcargo)
        {
            return rolesDAL.DeslinkearGrupoDeCargo(idgrupo, idcargo);
        }
    }
}
