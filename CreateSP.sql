USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[AltaProducto]    Script Date: 19/7/2021 10:15:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[AltaProducto]
@cod varchar(50), @desc varchar(100), @img varchar(200), @precio float
as
begin
INSERT INTO [dbo].[Producto]
           ([Codigo]
           ,[Descrip]
           ,[Imagen]
           ,[Precio])
     VALUES
           (@cod, @desc,@img,@precio)
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[AltaUsuario]    Script Date: 19/7/2021 10:15:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[AltaUsuario]
@legajo varchar(50), @name varchar(50), @lastname varchar(50),
@user varchar(50), @pw varchar(100), @email varchar(50), @rol int, @dvh varchar(500)
as
begin
INSERT INTO Usuario
           (id
           ,[Nombre]
           ,[Apellido]
           ,[EMail]
           ,[Username]
           ,[PW]
           ,[Rol]
		   ,[DVH])
     VALUES
           (@legajo, @name, @lastname,@email, @user,@pw,@rol,@dvh)
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[assignPermissionToGroup]    Script Date: 19/7/2021 10:15:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[assignPermissionToGroup]
@familia int, @pat int
as
begin
	
INSERT INTO [dbo].[Familia-Patente]
           (FamiliaId, PatenteId)
     VALUES
           (@familia, @pat)
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[assignPermissionToRole]    Script Date: 19/7/2021 10:15:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create proc [dbo].[assignPermissionToRole]
@pat int, @rol int
as
begin
INSERT INTO [dbo].[Rol-Patente]
           (rolId,PatenteId)
     VALUES
           (@rol,@pat)
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[BajaUsuario]    Script Date: 19/7/2021 10:15:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create proc [dbo].[BajaUsuario]
@id int
as
begin
DELETE FROM [dbo].[Usuario]
      WHERE Id =@id
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[CrearTipos]    Script Date: 19/7/2021 10:15:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[CrearTipos]
@tipo int, @desc varchar(50)
as
begin
	INSERT INTO [dbo].[BtMsgType]
           ([MsgType]
           ,[Descript])
     VALUES
           (@tipo, @desc)
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[DetachGroupFromRole]    Script Date: 19/7/2021 10:15:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[DetachGroupFromRole]
@fam int, @rol int
as
begin
	delete from [Rol-Familia]
	where [Rol-Familia].FamiliaId = @fam and [Rol-Familia].RolId =@rol
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[DetachPermissionFromRole]    Script Date: 19/7/2021 10:16:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[DetachPermissionFromRole]
 @pat int, @rol int
as
begin
	delete from [Rol-Patente]
	where [Rol-Patente].PatenteId = @pat and [Rol-Patente].RolId = @rol
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[DetachPermissionToGroup]    Script Date: 19/7/2021 10:16:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[DetachPermissionToGroup]
@fam int, @pat int
as
begin
	delete from [Familia-Patente] where [Familia-Patente].FamiliaId=@fam and [Familia-Patente].PatenteId = @pat
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[EraseAllLogs]    Script Date: 19/7/2021 10:16:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[EraseAllLogs]
as
begin
DELETE FROM [dbo].[Bitacora]
end


GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[eraseLogsByDate]    Script Date: 19/7/2021 10:17:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[eraseLogsByDate]
@fecha datetime
as
begin

DELETE FROM [dbo].[Bitacora]
      where FechaHora between @fecha +' 00:00:00.000' and @fecha + ' 23:59:59.999'
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getCargos]    Script Date: 19/7/2021 10:17:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create proc [dbo].[getCargos]
as
begin
select * from Rol
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getDVH]    Script Date: 19/7/2021 10:17:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[getDVH]
@id int
as
begin
	select DVH from Usuario
	where Id = @id
	order by Id
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getDVHs]    Script Date: 19/7/2021 10:17:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[getDVHs]
as
begin
	select DVH from Usuario
	order by Id
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getGroupsFromRole]    Script Date: 19/7/2021 10:17:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[getGroupsFromRole]
@rol int
as
begin
	select G.* from [dbo].[Rol-Familia] as RF
	inner join Familia as G on G.FamiliaId = RF.FamiliaId
	where RF.RolId = @rol
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getLogs]    Script Date: 19/7/2021 10:17:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Script for SelectTopNRows command from SSMS  ******/
CREATE proc [dbo].[getLogs]
as
begin
SELECT [FechaHora]
      ,[Message]
      ,[TipoMsg]
	  ,bt.Descript
  FROM [RunnerShop].[dbo].[Bitacora]
  inner join BtMsgType as bt on bt.MsgType = TipoMsg
  order by FechaHora desc
  end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getLogsByDate]    Script Date: 19/7/2021 10:17:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[getLogsByDate]
@fecha datetime
as
begin
SELECT [FechaHora]
      ,[Message]
      ,[TipoMsg]
	  ,bt.Descript
  FROM [RunnerShop].[dbo].[Bitacora]
  inner join BtMsgType as bt on bt.MsgType = TipoMsg
  where FechaHora between @fecha +' 00:00:00.000' and @fecha + ' 23:59:59.999'
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getPermissionsFromGroup]    Script Date: 19/7/2021 10:17:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[getPermissionsFromGroup]
@familia int
as
begin
	select P.* from Patente as P
	inner join [Familia-Patente] as GP on GP.PatenteId = P.PatenteId
	where GP.FamiliaId = @familia
end

GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getPermissionsFromRole]    Script Date: 19/7/2021 10:17:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[getPermissionsFromRole]
@rol int
as
begin
	select P.* from [Rol-Patente] as CP
	inner join Patente as P on P.PatenteId=CP.PatenteId
	where CP.RolId= @rol
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getProducto]    Script Date: 19/7/2021 10:17:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
create proc [dbo].[getProducto]
@cod varchar(50)
as
begin
SELECT [Codigo]
      ,[Descrip]
      ,[Imagen]
      ,[Precio]
  FROM [RunnerShop].[dbo].[Producto]
  where Codigo = @cod
  end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[getProductos]    Script Date: 19/7/2021 10:17:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
create proc [dbo].[getProductos]
as
begin
SELECT [Codigo]
      ,[Descrip]
      ,[Imagen]
      ,[Precio]
  FROM [RunnerShop].[dbo].[Producto]
  end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[GetRole]    Script Date: 19/7/2021 10:18:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[GetRole]
@rol int
as
begin
select * from Rol
where RolId = @rol
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[GetRolebyName]    Script Date: 19/7/2021 10:18:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[GetRolebyName]
@rol varchar(50)
as
begin
select * from Rol
where RolName = @rol
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[GetUser]    Script Date: 19/7/2021 10:18:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[GetUser]
@user varchar(50)
as
begin
	select U.*, R.RolName, R.RolDesc from Usuario as U
inner join Rol as R on R.RolId = U.Rol
where U.Username=@user
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[ListarUsuarios]    Script Date: 19/7/2021 10:18:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[ListarUsuarios]
as
begin
	select U.*, R.RolName, R.RolDesc from Usuario as U
inner join Rol as R on R.RolId = U.Rol
order by U.Id asc
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[LogBitacora]    Script Date: 19/7/2021 10:18:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[LogBitacora]
@fechahora datetime, @mensaje varchar(150), @tipo int
AS
BEGIN	
INSERT INTO [dbo].[Bitacora]
           ([FechaHora]
           ,[Message]
           ,[TipoMsg])
     VALUES
           (@fechahora, @mensaje, @tipo)
END
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[ModifCargo]    Script Date: 19/7/2021 10:18:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[ModifCargo]
@id int, @desc varchar(50), @detalle varchar(150)
as
begin
UPDATE [dbo].Rol
   SET [RolName] =@desc
      ,[RolDesc] =@detalle
 WHERE RolId=@id
end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[modiGrupo]    Script Date: 19/7/2021 10:18:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[modiGrupo]
@id int, @nombre varchar(50),@desc varchar(150)
as
begin

UPDATE [dbo].Familia
   SET FamiliaName =@nombre
      ,FamiliaDesc = @desc
 WHERE FamiliaId=@id
 end
GO

USE [RunnerShop]
GO

/****** Object:  StoredProcedure [dbo].[ModiUser]    Script Date: 19/7/2021 10:18:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[ModiUser]
@id int, @nombre varchar(50), @apellido varchar(50), @email varchar(50), 
@user varchar(50), @pw varchar(100),@rol int, @dvh varchar(500)
as
begin
UPDATE [dbo].[Usuario]
   SET [Nombre] = @nombre
      ,[Apellido] =@apellido
      ,[EMail] = @email
      ,[Username] = @user
      ,[PW] = @pw
      ,[Rol] = @rol
      ,[DVH] = @dvh
 WHERE Usuario.Id =@id
end


GO

USE [master]
GO

/****** Object:  StoredProcedure [dbo].[BackUpRunnerShop]    Script Date: 19/7/2021 10:18:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[BackUpRunnerShop]
@path varchar(700)
as
begin
declare @fullpath varchar(700) = 'C:\Users\jp-sa\Documents\archivos importantes\UAI\4to a�o\TP RunnerShop - Interfaz 2\BackUps_DB\' + @path + '.bat'
BACKUP DATABASE RunnerShop
TO DISK = @fullpath
end
GO

USE [master]
GO

/****** Object:  StoredProcedure [dbo].[RestoreRunnerShop]    Script Date: 19/7/2021 10:19:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[RestoreRunnerShop]
@path varchar(700)
as
begin
	declare @fullpath varchar(700) = 'C:\Users\jp-sa\Documents\archivos importantes\UAI\4to a�o\TP RunnerShop - Interfaz 2\BackUps_DB\' + @path
	alter database [RunnerShop] 
	set offline with rollback immediate
		RESTORE DATABASE [RunnerShop]
		FROM DISK = @fullpath
		WITH REPLACE
	alter database [RunnerShop] 
	set online
end
GO


