﻿using BussinesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DBSecurityAdmin
    {
        private readonly Acceso acceso = new Acceso();
        private readonly AccesoMaster accesoMaestro = new AccesoMaster();
        public string GetDVHsString()
        {
            string hash = "";
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("GetDVHs");
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                hash += row[0].ToString();
            }
            return hash;
        }
        public List<string> GetDVHs()
        {
            List<string> list = new List<string>();
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("GetDVHs");
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                list.Add(row[0].ToString());
            }
            return list;
        }
        public string GetDVH(Usuario u)
        // traer el DVH fde la base de datos
        {
            string dvh = "";
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("GetDVH", acceso.CrearParametro("@id", u.ID));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                dvh = row[0].ToString();
            }
            return dvh;
        }

        public List<string> GetFilesAndDVHs(string Separador)
        {
            List<string> digitos = new List<string>();
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("GetUsersFilesAndDVHs");
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                digitos.Add(row[0].ToString() + Separador + row[1].ToString());
            }
            return digitos;
        }
        public int CreateDB()
        {
            acceso.AbrirConexion();
            int rto = acceso.Escribir("CrearKitchen", new SqlParameter { }, CommandType.StoredProcedure);
            rto += acceso.Escribir("CrearKitchenInsightint", new SqlParameter { }, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
        public int BackUp(string FileName)
        {
            int rto = accesoMaestro.Escribir("BackUpRunnerShop", accesoMaestro.CrearParametro("@path", FileName), CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
        public int Restore(string FileName)
        {
            int rto = accesoMaestro.Escribir("RestoreRunnerShop", accesoMaestro.CrearParametro("@path", FileName), CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
    }
}
