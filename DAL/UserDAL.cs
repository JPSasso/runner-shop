﻿using BussinesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UserDAL
    {
        private readonly Acceso acceso = new Acceso();

        public int AltaUsuario(Usuario U, string dvh)
        {
            int rto;
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //  @legajo varchar(50), @name varchar(50), @lastname varchar(50),
                //  @user varchar(50), @pw varchar(100), @email varchar(50), @rol int
                acceso.CrearParametro("@legajo", U.ID),
                acceso.CrearParametro("@name",U.Nombre),
                acceso.CrearParametro("@lastname",U.Apellido),
                acceso.CrearParametro("@user", U.Username),
                acceso.CrearParametro("@pw", U.PassWord),
                acceso.CrearParametro("@email", U.EMail),
                acceso.CrearParametro("@rol", U.PermisoDeUsuario.ID),
                acceso.CrearParametro("@dvh", dvh)
            };
            rto = acceso.Escribir("AltaUsuario", parameters, System.Data.CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public List<Usuario> GetUsuarios()
        {
            List<Usuario> usuarios = new List<Usuario>();
            acceso.AbrirConexion();
            DataTable reader = acceso.Leer("ListarUsuarios");
            acceso.CerrarConexion();
            foreach (DataRow row in reader.Rows)
            {
                usuarios.Add(new Usuario
                {
                    ID = int.Parse(row[0].ToString()),
                    Nombre = row[1].ToString(),
                    Apellido = row[2].ToString(),
                    EMail = row[3].ToString(),
                    Username = row[4].ToString(),
                    PassWord = row[5].ToString(),
                    PermisoDeUsuario = new Rol(int.Parse(row[6].ToString()), row[8].ToString(), row[9].ToString())
                });

            }
            return usuarios;
        }

        public int BloquearUsuario(string username)
        {
            acceso.AbrirConexion();
            int rto = acceso.Escribir("UPDATE [dbo].[Usuario] SET " +
                " [U_Bloqueado] = 1 WHERE  Usuario.Username = '" + username.ToString() + "'",
                CommandType.Text);
            acceso.CerrarConexion();
            return rto;
        }
        public int DesbloquearUsuario(string username)
        {
            acceso.AbrirConexion();
            int rto = acceso.Escribir("UPDATE [dbo].[Usuario] SET " +
                " [U_Bloqueado] = 0 WHERE Usuario.Username = '" + username.ToString() + "'",
                CommandType.Text);
            acceso.CerrarConexion();
            return rto;
        }
        public Rol GetUserRole(Usuario u)
        {
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("getPermissionsFromRole", acceso.CrearParametro("@rol", u.PermisoDeUsuario.ID));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                u.PermisoDeUsuario.AddPatente(new Patente(int.Parse(row[0].ToString()),
                    row[1].ToString(), row[2].ToString()));
            }
            table = null;
            acceso.AbrirConexion();
            table = acceso.LeerUno("getGroupsFromRole", acceso.CrearParametro("@rol", u.PermisoDeUsuario.ID));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                u.PermisoDeUsuario.AddFamilia(new Familia(int.Parse(row[0].ToString()),
                    row[1].ToString(), row[2].ToString()));
            }
            acceso.AbrirConexion();
            foreach (Familia F in (List<Permiso>)u.PermisoDeUsuario.DisplayFamilias())
            {
                table = acceso.LeerUno("getPermissionsFromGroup", acceso.CrearParametro("@familia", F.ID));
                foreach (DataRow row in table.Rows)
                {
                    F.Add(new Patente(int.Parse(row[0].ToString()),
                        row[1].ToString(), row[2].ToString()));
                }
                table = null;
            }
            acceso.CerrarConexion();
            return u.PermisoDeUsuario;
        }

        public Usuario GetUsuario(string Username)
        {
            Usuario U = new Usuario();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@legajo nvarchar(50) = NULL, @user nvarchar(50) = NULL
                acceso.CrearParametro("@user",Username)
            };
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("GetUser", parameters);
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                U.ID = int.Parse(row[0].ToString());
                U.Nombre = row[1].ToString();
                U.Apellido = row[2].ToString();
                U.EMail = row[3].ToString();
                U.Username = row[4].ToString();
                U.PassWord = row[5].ToString();
                U.PermisoDeUsuario = new Rol(int.Parse(row[6].ToString()), row[7].ToString(), row[8].ToString());
            }
            return U;
        }

        public int ModifUsuario(Usuario U, string dvh)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                    // @id int, @nombre varchar(50), @apellido varchar(50), @email varchar(50),
                    // @user varchar(50), @pw varchar(100),@rol int, @dvh varchar(100)
                acceso.CrearParametro("@id", U.ID),
                acceso.CrearParametro("@nombre",U.Nombre),
                acceso.CrearParametro("@apellido",U.Apellido),
                acceso.CrearParametro("@email", U.EMail),
                acceso.CrearParametro("@user", U.Username),
                acceso.CrearParametro("@pw",U.PassWord),
                acceso.CrearParametro("@rol", U.PermisoDeUsuario.ID),
                acceso.CrearParametro("@dvh", dvh)
            };
            acceso.AbrirConexion();
            int rto = acceso.Escribir("ModiUser", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
        public int BajaUsuario(Usuario user)
        {
            List<SqlParameter> parameters = new List<SqlParameter> {
            acceso.CrearParametro("@legajo",user.ID)};
            acceso.AbrirConexion();
            int rto = acceso.Escribir("BajaUsuario", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

    }
}
