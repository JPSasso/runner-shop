﻿using BussinesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class RolesDAL
    {
        private readonly Acceso acceso = new Acceso();
        public List<Permiso> GetRoles()
        {
            List<Permiso> Roles = new List<Permiso>();
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("getCargos");
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                Roles.Add(new Rol(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString()));
            }
            foreach (Rol rol in Roles)
            {
                acceso.AbrirConexion();
                table = acceso.LeerUno("getGroupsFromRole", acceso.CrearParametro("@rol", rol.ID));
                acceso.CerrarConexion();
                foreach (DataRow row in table.Rows)
                {
                    Familia F = new Familia(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString());
                    acceso.AbrirConexion();
                    DataTable groupTable = acceso.LeerUno("getPermissionsFromGroup", acceso.CrearParametro("@familia", int.Parse(row[0].ToString())));
                    acceso.CerrarConexion();
                    foreach (DataRow row1 in groupTable.Rows)
                    {
                        F.Add(new Patente(int.Parse(row1[0].ToString()), row1[1].ToString(), row1[2].ToString()));
                    }
                    rol.AddFamilia(F);
                }
            }
            foreach (Rol rol in Roles)
            {
                acceso.AbrirConexion();
                table = acceso.LeerUno("getPermissionsFromRole", acceso.CrearParametro("@rol", rol.ID));
                acceso.CerrarConexion();
                foreach (DataRow row in table.Rows)
                {
                    rol.AddPatente(new Patente(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString()));
                }
            }
            return Roles;
        }

        public Rol GetRole(string id)
        {
            int rolid = -2;
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("GetRolebyName", acceso.CrearParametro("@rol", id));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                rolid = int.Parse(row[0].ToString());
            }
            return GetRole(rolid);
        }

        public Rol GetRole(int id)
        {
            Rol rol = null;
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("GetRole", acceso.CrearParametro("@rol", id));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                rol = new Rol(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString());
            }
            acceso.AbrirConexion();
            table = acceso.LeerUno("getGroupsFromRole", acceso.CrearParametro("@rol", rol.ID));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                Familia F = new Familia(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString());
                acceso.AbrirConexion();
                DataTable groupTable = acceso.LeerUno("getPermissionsFromGroup", acceso.CrearParametro("@familia", int.Parse(row[0].ToString())));
                acceso.CerrarConexion();
                foreach (DataRow row1 in groupTable.Rows)
                {
                    F.Add(new Patente(int.Parse(row1[0].ToString()), row1[1].ToString(), row1[2].ToString()));
                }
                rol.AddFamilia(F);
            }
            acceso.AbrirConexion();
            table = acceso.LeerUno("getPermissionsFromRole", acceso.CrearParametro("@rol", rol.ID));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                rol.AddPatente(new Patente(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString()));
            }
            return rol;
        }
        public int LinkearPermisoACargo(int idPatente, int idRol)
        {
            int rto;
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@pat",idPatente),
                acceso.CrearParametro("@rol",idRol)
            };
            acceso.AbrirConexion();
            rto = acceso.Escribir("assignPermissionToRole", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int DeslinkearGrupoDeCargo(int idFamilia, int idrol)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@fam",idFamilia),
                acceso.CrearParametro("@rol",idrol)
            };
            acceso.AbrirConexion();
            int rto = acceso.Escribir("DetachGroupFromRole", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;

        }

        public List<Permiso> GetPermisosFromCargo(int iD)
        {
            List<Permiso> permisos = new List<Permiso>();
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("getPermissionsFromRole", acceso.CrearParametro("@rol", iD));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                permisos.Add(new Patente(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString()));
            }
            return permisos;
        }

        public List<Permiso> GetRolesFromCargo(int idcargo)
        {
            List<Permiso> grupos = new List<Permiso>();
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("getGroupsFromRole", acceso.CrearParametro("@rol", idcargo));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                grupos.Add(new Familia(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString()));
            }
            return grupos;
        }

        public int LinkearGrupoACargo(int idFamilia, int IdRol)
        {
            int rto;
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@fam",idFamilia),
                acceso.CrearParametro("@rol",IdRol)
            };
            acceso.AbrirConexion();
            rto = acceso.Escribir("assignGroupToRole", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
        public int DeslinkearPermisoAGrupo(int idPermiso, int idFamilia)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@fam",idFamilia),
                acceso.CrearParametro("@perm",idPermiso)
            };
            acceso.AbrirConexion();
            int rto = acceso.Escribir("DetachPermissionToGroup", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
        public int DeslinkearPermisoACargo(int idPermiso, int idcargo)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@rol",idcargo),
                acceso.CrearParametro("@perm",idPermiso)
            };
            acceso.AbrirConexion();
            int rto = acceso.Escribir("DetachPermissionFromRole", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
        public int EraseCargo(Rol R)
        {
            int rto;
            acceso.AbrirConexion();
            rto = acceso.Escribir("EraseCargo", acceso.CrearParametro("@id", R.ID), CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int ModifCargo(Rol R)
        {
            int rto;
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@id",R.ID),
                acceso.CrearParametro("@desc",R.Nombre),
                acceso.CrearParametro("@detalle",R.Descripcion)
            };
            acceso.AbrirConexion();
            rto = acceso.Escribir("ModifCargo", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int AddCargo(Rol R)
        {
            int rto;
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@id",R.ID),
                acceso.CrearParametro("@desc",R.Nombre),
                acceso.CrearParametro("@detalle",R.Descripcion)
            };
            acceso.AbrirConexion();
            rto = acceso.Escribir("addCargo", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int BorrarGrupo(Familia F)
        {
            int rto;
            acceso.AbrirConexion();
            rto = acceso.Escribir("EraseGroup", acceso.CrearParametro("@id", F.ID), CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int ModifGrupo(Familia F)
        {
            int rto;
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@id",F.ID),
                acceso.CrearParametro("@nombre",F.Nombre),
                acceso.CrearParametro("@desc",F.Descripcion)
            };
            acceso.AbrirConexion();
            rto = acceso.Escribir("modiGrupo", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public List<Permiso> GetPermisosDeGrupo(int IdGrupo)
        {
            List<Permiso> permisos = new List<Permiso>();
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("getPermissionsFromGroup", acceso.CrearParametro("@fam", IdGrupo));
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                permisos.Add(new Patente(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString()));
            }
            return permisos;
        }

        public int LinkearPermisoAGrupo(int idPatente, int IdFamilai)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {acceso.CrearParametro("@fam",IdFamilai),
            acceso.CrearParametro("@perm",idPatente)
            };
            acceso.AbrirConexion();
            int rto = acceso.Escribir("assignPermissionToGroup", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int BorrarPermiso(Permiso p)
        {
            int rto;
            rto = acceso.Escribir("erasePermission", acceso.CrearParametro("@id", p.ID), CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int AltaGrupo(Familia F)
        {
            int rto;
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@id",F.ID),
                acceso.CrearParametro("@nombre",F.Nombre),
                acceso.CrearParametro("@desc",F.Descripcion)
            };
            rto = acceso.Escribir("addGroup", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
        public object getGrupos()
        {
            List<Permiso> grupos = new List<Permiso>();
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("getGroups", null);
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                grupos.Add(new Familia(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString()));
            }
            return grupos;
        }

        public int AltaPermiso(Permiso p)
        {
            int rto;
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@id",p.ID),
                acceso.CrearParametro("@nombre",p.Nombre),
                acceso.CrearParametro("@desc",p.Descripcion)
            };
            acceso.AbrirConexion();
            rto = acceso.Escribir("addPermissions", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public List<Permiso> GetPermisos()
        {
            List<Permiso> permisos = new List<Permiso>();
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("getPermissions", null);
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                permisos.Add(new Patente(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString()));
            }
            return permisos;
        }
    }
}


