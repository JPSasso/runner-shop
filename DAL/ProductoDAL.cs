﻿using BussinesEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ProductoDAL
    {
        Acceso acceso = new Acceso();

        public List<Producto> GetProductos()
        {
            List<Producto> productos = new List<Producto>();
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("getProductos");
            foreach (DataRow row in table.Rows)
            {
                productos.Add(new Producto
                {
                    Codigo = row[0].ToString(),
                    descripcion = row[1].ToString(),
                    imagen = row[2].ToString(),
                    Precio = float.Parse(row[3].ToString())
                });
            }
            acceso.CerrarConexion();
            return productos;
        }

        public Producto GetProducto(string productCode)
        {
            Producto P = null;
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("getProducto", acceso.CrearParametro("@cod", productCode));
            foreach (DataRow row in table.Rows)
            {
                P = new Producto
                {
                    Codigo = row[0].ToString(),
                    descripcion = row[1].ToString(),
                    imagen = row[2].ToString(),
                    Precio = float.Parse(row[3].ToString())
                };
            }
            acceso.CerrarConexion();
            return P;
        }

        public int AltaProducto(Producto p)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@cod", p.Codigo),
                acceso.CrearParametro("@desc", p.descripcion),
                acceso.CrearParametro("@img",p.imagen),
                acceso.CrearParametro("@precio",p.Precio)
            };
            acceso.AbrirConexion();
            int rto = acceso.Escribir("AltaProducto", parameters, CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
    }
}
