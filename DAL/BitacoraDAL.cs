﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class BitacoraDAL
    {
        private readonly Acceso acceso = new Acceso();
        public int Log(DateTime fechaHora, string message, int codtipoMensaje)
        {
            //@fechahora datetime, @mensaje varchar(50), @tipo int
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@fechahora", fechaHora),
                acceso.CrearParametro("@mensaje",message),
                acceso.CrearParametro("@tipo", codtipoMensaje)
            };
            int rto = acceso.Escribir("LogBitacora", parameters, System.Data.CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int PersistirTipos(Dictionary<int, string> mensajes)
        {
            int rto = 0;
            //@tipo int, @desc varchar(150)
            List<SqlParameter> parameters = new List<SqlParameter>();
            foreach (var tipo in mensajes)
            {
                parameters.Add(acceso.CrearParametro("@tipo", tipo.Key));
                parameters.Add(acceso.CrearParametro("@desc", tipo.Value));
                acceso.AbrirConexion();
                rto = acceso.Escribir("CrearTipos", parameters, System.Data.CommandType.StoredProcedure);
                parameters.Clear();
            }
            acceso.CerrarConexion();
            return rto;
        }

        public List<DataRow> GetLogs()
        {
            List<DataRow> logs = new List<DataRow>();
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("getLogs", null);
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                logs.Add(row);
            }
            return logs;
        }

        public List<DataRow> GetLogs(DateTime dateTime)
        {
            List<DataRow> logs = new List<DataRow>();
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                acceso.CrearParametro("@fecha",dateTime.ToString("yyyy-MM-dd"))
            };
            acceso.AbrirConexion();
            DataTable table = acceso.Leer("getLogsByDate", parameters);
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)
            {
                logs.Add(row);
            }
            return logs;
        }

        public int EraseAllLogs()
        {
            int rto = acceso.Escribir("EraseAllLogs", CommandType.StoredProcedure);
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@fechahora", DateTime.Now),
                acceso.CrearParametro("@mensaje",DateTime.Now.ToString("yy-MM-dd HH:mm:ss") +": Se elimino la bitacora"),
                acceso.CrearParametro("@tipo", 1)
            };
            acceso.Escribir("LogBitacora", parameters, System.Data.CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }

        public int EraseLogs(DateTime dateTime)
        {
            int rto = acceso.Escribir("eraseLogsbyDate",
                acceso.CrearParametro("@fecha", dateTime.ToString("yyyy-MM-dd")),
                CommandType.StoredProcedure);
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@fechahora", DateTime.Now),
                acceso.CrearParametro("@mensaje",DateTime.Now.ToString("yy-MM-dd HH:mm:ss") +": Se elimino la bitacora del dia " + dateTime.ToString("yyyy-MM-dd")),
                acceso.CrearParametro("@tipo", 1)
            };
            acceso.Escribir("LogBitacora", parameters, System.Data.CommandType.StoredProcedure);
            acceso.CerrarConexion();
            return rto;
        }
    }
}
