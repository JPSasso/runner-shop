﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Seguridad_Servicios
{
    public class CryptoManager
    {
        public bool Compare(string Userpassword, string testedpassword)
        {
            return (Userpassword == ApplyHash(testedpassword));
        }

        private readonly string Hash = "4Cc3D3r4@=Runn3r5h0p?";

        public string ApplyHash(string password)
        {
            string outcome = "";

            byte[] data = UTF8Encoding.UTF8.GetBytes(password);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(Hash));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider()
                {
                    Key = keys,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                })
                {
                    ICryptoTransform transform = tripDes.CreateEncryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    outcome = Convert.ToBase64String(results, 0, results.Length);
                };
            }
            return outcome;
        }
        private string Decrypt(string pstring)
        {
            string outcome = "";

            byte[] data = Convert.FromBase64String(pstring);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(Hash));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider()
                {
                    Key = keys,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                })
                {
                    ICryptoTransform transform = tripDes.CreateDecryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    outcome = UTF8Encoding.UTF8.GetString(results);
                };
            }
            return outcome;
        }
    }
}
