﻿using BussinesEntity;
using DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seguridad_Servicios
{
    public class DataIntegrityManager
    {
        readonly CryptoManager CM = new CryptoManager();
        readonly DBSecurityAdmin dBSecurityAdmin = new DBSecurityAdmin();

        // cuando se actualiza la BD
        public readonly string SnapShotRoute = @"C:\Users\jp-sa\Documents\archivos importantes\UAI\4to año\TP RunnerShop - Interfaz 2\Reports\SnapShot.txt";
        public readonly string SnapDVV = @"C:\Users\jp-sa\Documents\archivos importantes\UAI\4to año\TP RunnerShop - Interfaz 2\Reports\SnapDVV.txt";
        // obtener los registros de la bd para recalcular
        public readonly string DBExportRoute = @"C:\Users\jp-sa\Documents\archivos importantes\UAI\4to año\TP RunnerShop - Interfaz 2\Reports\Export.txt";
        public readonly string ExportDVV = @"C:\Users\jp-sa\Documents\archivos importantes\UAI\4to año\TP RunnerShop - Interfaz 2\Reports\ExportDVV.txt";
        // errores
        public readonly string ReportRoute = @"C:\Users\jp-sa\Documents\archivos importantes\UAI\4to año\TP RunnerShop - Interfaz 2\Reports\Report.txt";

        public readonly string DbBackUpRoute = @"C:\Users\jp-sa\Documents\archivos importantes\UAI\4to año\TP RunnerShop - Interfaz 2\BackUps_DB\";

        private const string Separador = "###";
        private const int Divisor = 10;

        private FileStream OpenSnapFile;
        private FileStream OpenExtractFile;

        public FileStream GetSnapShotFile()
        {
            OpenSnapFile = File.OpenRead(SnapShotRoute);
            return OpenSnapFile;
        }
        public void CreateSnapShotFile()
        {
            CrearArchivodeSeguridad(new StreamWriter(SnapShotRoute));
        }

        public FileStream GetExtractFile()
        {
            OpenExtractFile = File.OpenRead(DBExportRoute);
            return OpenExtractFile;
        }
        public void CreateExtractFile()
        {
            StreamWriter sw = new StreamWriter(DBExportRoute);
            foreach (Usuario u in new UserDAL().GetUsuarios())
            {
                sw.WriteLine(u.ID + Separador + CM.ApplyHash(u.ToString()));
            }
            sw.Close();
        }

        public void Release()
        {
            if (OpenSnapFile != null)
            {
                OpenSnapFile.Close();
                OpenExtractFile.Close();
            }
        }
        public int LoadDB()
        {
            return dBSecurityAdmin.CreateDB();
        }

        public int BackUp(string path)
        {
            return dBSecurityAdmin.BackUp(path);
        }
        public int Restore(string path)
        {
            return dBSecurityAdmin.Restore(path);
        }
        public string GetDVV()
        {
            return CalcularDVV();
        }
        public List<string> GetDVH()
        {
            List<string> rto = VerifyDVHs();
            if (rto[rto.Count - 1] == "Corrupted")
            {
                GenerarInformeDeCorrupciones(rto);
            }
            return rto;
        }
        public void CrearSnapshot()
        {
            CreateSnapshot();
        }
        private void CrearArchivodeSeguridad(StreamWriter sw)
        {
            List<Usuario> usuarios = new UserDAL().GetUsuarios();
            foreach (Usuario u in usuarios)
            {
                sw.WriteLine(u.ID.ToString() + Separador + dBSecurityAdmin.GetDVH(u));
            }
            sw.Close();
        }
        /// para evitar que el DVV sea excesivamente largo, se divide el largo por un numero "divisor" parametrizable,
        /// Y del string resultante de calcular el DVV, se toman los pimeros "Divisor" caracteres

        public List<string> GetFilesAndDVHs()
        /// trae los legajos de los empleados, junto con sus DVHs
        {
            return dBSecurityAdmin.GetFilesAndDVHs(Separador);
        }
        public void GenerarInformeDeCorrupciones(List<string> rto)
        {
            StreamWriter sw = new StreamWriter(ReportRoute);
            foreach (var s in from string s in rto
                              where s != "Corrupted"
                              select s)
            {
                sw.WriteLine(s);
            }

            sw.Close();
        }
        public string CalcularDVH(Usuario u)
        {
            return CM.ApplyHash(u.ToString());
        }
        public string CalcularDVV()
        {
            string hash = "";
            StreamWriter sw = new StreamWriter(ExportDVV);
            foreach (Usuario u in new UserDAL().GetUsuarios())
            {
                hash += u.ID.ToString() + Separador + dBSecurityAdmin.GetDVH(u);
            }
            string output = new CryptoManager().ApplyHash(hash);
            sw.WriteLine(output);
            sw.Close();
            return output;
        }
        public void createDvvSnapFile()
        {
            StreamWriter sw = new StreamWriter(SnapDVV);
            string hash = "";
            foreach (Usuario u in new UserDAL().GetUsuarios())
            {
                hash += u.ID.ToString() + Separador + dBSecurityAdmin.GetDVH(u);
            }
            string output = new CryptoManager().ApplyHash(hash);
            sw.WriteLine(output);
            sw.Close();
        }
        public void CreateSnapshot()
        {
            CreateSnapShotFile();
        }
        public List<string> VerifyDVHs()
        {
            CreateExtractFile();
            FileStream snap = GetSnapShotFile();
            FileStream extract = GetExtractFile();
            StreamReader snapreader = new StreamReader(SnapShotRoute);
            StreamReader extreader = new StreamReader(DBExportRoute);
            int match = 0;
            List<string> errores = new List<string>();
            if (snap.CanRead && extract.CanRead)
            {
                do
                {
                    string snaps = snapreader.ReadLine();
                    string extracts = extreader.ReadLine();
                    if (snaps == extracts)
                    {
                        if (match != -1)
                        {
                            match = 0; //archivo ok
                        }
                    }
                    else
                    {
                        match = -1; //archivo corrompido
                        errores.Add(extracts);
                    }
                }
                while (snapreader.EndOfStream is false);
            }
            else
            {
                match = -2; //archivo inaccesible
            }
            snapreader.Close();
            extreader.Close();
            Release();
            switch (match)
            {
                case 0:
                    errores.Add("checked"); break;
                case -1:
                    errores.Add("Corrupted"); break;
                case -2:
                    errores.Add("Missing File"); break;
                default:
                    break;
            }
            return errores;
        }
        public string VerifyDVVs()
        {
            CalcularDVV();
            FileStream snap = File.OpenRead(SnapDVV);
            FileStream extract = File.OpenRead(ExportDVV);
            StreamReader snapreader = new StreamReader(SnapDVV);
            StreamReader extreader = new StreamReader(ExportDVV);
            int match = 0;
            if (snap.CanRead && extract.CanRead)
            {
                do
                {
                    string snaps = snapreader.ReadLine();
                    string extracts = extreader.ReadLine();
                    if (snaps == extracts)
                    {
                        if (match != -1)
                        {
                            match = 0; //archivo ok
                        }
                    }
                    else
                    {
                        match = -1; //archivo corrompido
                    }
                }
                while (snapreader.EndOfStream is false);
            }
            else
            {
                match = -2; //archivo inaccesible
            }
            snapreader.Close();
            extreader.Close();
            switch (match)
            {
                case 0:
                    return "checked";
                case -1:
                    return "Corrupted";
                case -2:
                    return "Missing File";
                default: return null;
            }
        }
    }
}
