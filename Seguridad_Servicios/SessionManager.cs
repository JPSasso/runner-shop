﻿using BussinesEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seguridad_Servicios
{
    public class SessionManager
    {
        private static SessionManager Session;
        private List<Usuario> usuariosList;
        private readonly Dictionary<int, Usuario> Users = new Dictionary<int, Usuario>();
        private static readonly object syncLock = new object();
        public List<Usuario> UsuariosList
        {
            get { return usuariosList; }
            set { usuariosList = value; }
        }

        private SessionManager()
        {

        }
        public void CloseSession()
        {
            Session = null;
        }
        public void SetUsuarios(List<Usuario> usuarios)
        {
            if (usuarios != null)
            {
                foreach (Usuario u in usuarios)
                {
                    try { Users.Add(u.ID, u); }
                    catch { }
                }
            }
            else usuariosList = null;
        }
        public static SessionManager GetSession()
        {
            if (Session is null)
            {
                lock (syncLock)
                {
                    if (Session is null)
                    {
                        Session = new SessionManager();
                    }
                }
            }
            return Session;
        }
        public Usuario GetUsuario(int doc)
        {
            return Users.TryGetValue(doc, out Usuario value) ? value : null;
        }
    }
}
