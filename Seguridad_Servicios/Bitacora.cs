﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seguridad_Servicios
{
    public class Bitacora
    {
        public BitacoraDAL dal = new BitacoraDAL();
        public enum TipoMensaje
        {
            // luego de agregar un tipo de mensaje, modificar la funcion "GuardarTipos()"
            Error = -1,
            Mensaje = 0,
            Advertencia = 1,
            Custom = 2
        }
        public void GuardarTipos()
        {
            Dictionary<int, string> Mensajes = new Dictionary<int, string>();
            for (int i = -1; i < 3; i++)
            {
                Mensajes.Add(i, ((TipoMensaje)i).ToString());
            }
            dal.PersistirTipos(Mensajes);
        }
        public void SaveDailyLogFile()
        {

        }

        public void CustomLog(Bitacora.TipoMensaje tipoMensaje, string msg)
        {
            dal.Log(DateTime.Now, DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": " + msg, (int)tipoMensaje);
        }
        public void SystemStart()
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": Se inicio el sistema",
                (int)TipoMensaje.Advertencia);
        }

        public List<string> GetLogs()
        {
            List<DataRow> objs = dal.GetLogs();
            List<string> logs = new List<string>();
            foreach (DataRow row in objs)
            {
                logs.Add(row[3].ToString() + ": " + row[1].ToString());
            }
            return logs;
        }

        public void SystemStop()
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": Se cerro el sistema",
                (int)TipoMensaje.Advertencia);
        }

        public int EraseLogs()
        {
            return dal.EraseAllLogs();
        }

        public void RegistroPermiso(string user)
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": Se modificaron los permisos del usuario " + user,
                (int)TipoMensaje.Advertencia);
        }

        public List<string> GetLogs(DateTime dateTime)
        {
            List<DataRow> objs = dal.GetLogs(dateTime);
            List<string> logs = new List<string>();
            foreach (DataRow row in objs)
            {
                logs.Add(row[3].ToString() + ": " + row[1].ToString());
            }
            return logs;
        }

        public void LogIn(string user)
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": El usuario " + user + " inicio sesion",
                (int)TipoMensaje.Mensaje);
        }
        public void LogOut(string user)
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": El usuario " + user + " cerro sesion",
                (int)TipoMensaje.Mensaje);
        }
        public void AltaUsuario(string user)
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": El usuario " + user + " se dio de alta en el sistema",
                (int)TipoMensaje.Mensaje);
        }
        public void ModiUsuario(string user)
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": El usuario " + user + " se modifico",
                (int)TipoMensaje.Mensaje);
        }

        public int EraseLogs(DateTime date)
        {
            return dal.EraseLogs(date);
        }

        public void BajaUsuario(string user)
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": El usuario " + user + " se dio de baja del sistema",
                (int)TipoMensaje.Mensaje);
        }
        public void Stats(string stats)
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": se actualizaron las estadisticas de " + stats,
                (int)TipoMensaje.Mensaje);
        }
        public void BackUp()
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": Se realizo un BackUp de la Base de Datos",
                (int)TipoMensaje.Advertencia);
        }
        public void Restore()
        {
            dal.Log(DateTime.Now,
                DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + ": Se realizo un Restore de la Base de Datos",
                (int)TipoMensaje.Advertencia);
        }
    }
}
