﻿using BLL;
using BussinesEntity;
using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RunnerShop
{
    public partial class webMaster : System.Web.UI.Page
    {
        Usuario user;
        DataIntegrityManager DIM = new DataIntegrityManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["DVV"] == null)
            {
                Session["DVV"] = DIM.GetDVV();
            }
            if (Session["Usuario"] != null)
            {
                string usuario = Session["Usuario"].ToString();
                user = new Usuario
                {
                    // return ID.ToString() +"_" + Nombre + "_" + Apellido + "_" +
                    // Username + "_" + PassWord + "_" + EMail + "_" + rol.ID.ToString();
                    ID = int.Parse(usuario.Split('_')[0]),
                    Nombre = usuario.Split('_')[1],
                    Apellido = usuario.Split('_')[2],
                    Username = usuario.Split('_')[3],
                    PassWord = usuario.Split('_')[4],
                    EMail = usuario.Split('_')[5],
                    PermisoDeUsuario = new RolesBLL().GetRol(int.Parse(usuario.Split('_')[6]))
                };
                lblUsuario.Text = user.Nombre + " " + user.Apellido;
            }
            else
            {

            }
        }

        protected void btnBitacora_Click(object sender, EventArgs e)
        {
            if (user.PermisoDeUsuario.ID == 0)
            {
                Session["UrlLog"] = true;
                Response.Redirect("bitacora.aspx");
            }
            else
            {
                Response.Write("<script>alert('Usted no posee los permisos necesarios para realizar esta accion');</script>");
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("backUp.aspx");
        }

        protected void btnVerificarH_Click(object sender, EventArgs e)
        {
            txtResultado.Text = string.Empty;
            List<string> rto = DIM.VerifyDVHs();
            string integridad = rto[rto.Count - 1].ToString();
            Session["DVH Result"] = integridad;
            if (integridad == "Corrupted")
            {
                foreach (string s in rto)
                {
                    if (s != "Corrupted")
                    {
                        txtResultado.Text += "Registro Nro " + s.Split('#')[0] + " Corrupto en la tabla [RunnerShop].[dbo].[Usuario]\r\n";
                        Session["error"] = true;
                    }
                }
            }
            else
            {
                labelRtoDVs.Visible = true;
                labelRtoDVs.Text = "Registros Horizontales checkeados correctamente";
                Session["error"] = false;
            }
        }

        protected void btnRecalcularH_Click(object sender, EventArgs e)
        {
            txtResultado.Text = string.Empty;
            List<Usuario> users = new UserBLL().GetUsuarios();
            foreach (Usuario usuario in users)
            {
                txtResultado.Text += DIM.CalcularDVH(usuario) + "\r\n";
                new UserBLL().ModifUsuario(usuario);
            }
            DIM.CreateSnapShotFile();
            labelRtoDVs.Visible = true;
            labelRtoDVs.Text = "Digitos recalculados!";
            Session["error"] = false;
        }

        protected void btnVerificarV_Click(object sender, EventArgs e)
        {
            txtResultado.Text = string.Empty;
            if (Session["DVV"] != null)
            {
                //---------------------------
                List<string> rto = DIM.VerifyDVHs();
                string integridad = rto[rto.Count - 1].ToString();
                Session["DVH Result"] = integridad;
                if (integridad == "Corrupted")
                {
                    foreach (string s in rto)
                    {
                        if (s != "Corrupted")
                        {
                            txtResultado.Text += "Verificacion Vertical:\r\nRegistro Nro " + s.Split('#')[0] + " Corrupto en la tabla [RunnerShop].[dbo].[Usuario]\r\n";
                            Session["error"] = true;
                        }
                    }
                }
                else
                {
                    labelRtoDVs.Visible = true;
                    labelRtoDVs.Text = "Registros Verticales checkeados correctamente";
                    Session["error"] = false;
                }
            }
            else
            {
                Response.Write("<script>alert('Error en lectura de digitos verificadores');</script>");
            }
        }

        protected void btnRecalcularV_Click(object sender, EventArgs e)
        {
            txtResultado.Text = string.Empty;

            string DVVactual = DIM.CalcularDVV();
            Session["DVV"] = DVVactual;
            txtResultado.Text = DVVactual;
            DIM.createDvvSnapFile();

            labelRtoDVs.Visible = true;
            labelRtoDVs.Text = "Digito vertical recalculado!";
            Session["error"] = false;
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }

        protected void btnNewuser_Click(object sender, EventArgs e)
        {
            Response.Redirect("AltaUsuario.aspx");
        }
    }
}