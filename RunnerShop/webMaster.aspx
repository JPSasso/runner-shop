﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webMaster.aspx.cs" Inherits="RunnerShop.webMaster" enableSessionState="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>RunnerShop</title>
    <link href="/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            width: 575px;
            margin-left: 435px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;" class="center">
                <tr>
                    <td class="aLeft">
                        <asp:Image ID="logo1" runat="server" ImageUrl="~/imagenes/logo.png" Height="40" ImageAlign="Middle" />
                    </td>
                    <td class="aRight">
                        <asp:Label ID="lblUsuario" runat="server" Text="usuario"></asp:Label>
                        &nbsp;
                        <asp:Button ID="btnNewuser" runat="server" Text="Dar de Alta usuario" Width="150px" CssClass="button" OnClick="btnNewuser_Click" />&nbsp;
                        <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="100px" CssClass="button" OnClick="btnCerrar_Click" />
                    </td>
                </tr>
            </table>

            <br />
            <asp:Label runat="server" ID="lblPermiso" Text="Perfil: Webmaster" Font-Bold="True" Font-Size="Medium" CssClass="aCenter"></asp:Label>
            <br /><br />
            <div class="auto-style1">
                <asp:Label runat="server" ID="labelRtoDVs" Text="rto" ForeColor="Red" CssClass="aCenter" Font-Bold="False" Visible="False"></asp:Label>
            </div>            
            <br />
            <br />
            <table style="width: 40%;" class="center">
                 <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Ver bitacora"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnBitacora" runat="server" Text="Ejecutar" OnClick="btnBitacora_Click" CssClass="button" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Copia de seguridad y Restauración"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Ejecutar" OnClick="btnBack_Click" CssClass="button" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Verificar integridad Horizontal"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnVerificarH" runat="server" Text="Ejecutar" OnClick="btnVerificarH_Click" CssClass="button" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Recalcular Digitos Horizontales"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnRecalcularH" runat="server" Text="Ejecutar" OnClick="btnRecalcularH_Click" CssClass="button" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Verificar integridad Vertical"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnVerificarV" runat="server" Text="Ejecutar" OnClick="btnVerificarV_Click" CssClass="button" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="Recalcular Digitos Verticales"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnRecalcularV" runat="server" Text="Ejecutar" OnClick="btnRecalcularV_Click" CssClass="button" />
                    </td>
                </tr>
            </table>

            <br />
            <br />

            <table style="width: 900px;" class="center">
                <tr>
                    <td>
                        <asp:TextBox ID="txtResultado" runat="server" ReadOnly="True" TextMode="MultiLine" Width="900px" Height="200px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
