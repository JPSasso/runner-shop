﻿using BLL;
using BussinesEntity;
using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RunnerShop
{
    public partial class AltaProductos : System.Web.UI.Page
    {
        ProductoBLL productoBLL = new ProductoBLL();
        Bitacora Log = new Bitacora();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            Producto P = new Producto
            {
                Codigo = txtCod.Text,
                descripcion = txtDesc.Text,
                Precio = float.Parse(txtPrice.Text),
                imagen = FileUpload1.FileName
            };
            if (productoBLL.altaProducto(P) > 0)
            {
                lblError.Text = P.Codigo + " dado de alta correctamente";
                Log.CustomLog(Bitacora.TipoMensaje.Mensaje, "Se dio de alta el producto " + P.Codigo + "->" + P.descripcion);
            }
            else
            {
                lblError.Text = "Error en el alta";
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("administrador.aspx");
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            Response.Redirect("administrador.aspx");
        }
    }
}