﻿using BLL;
using BussinesEntity;
using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RunnerShop
{
    public partial class Home : System.Web.UI.Page
    {
        private readonly Bitacora LOG = new Bitacora();
        private readonly UserBLL userBLL = new UserBLL();
        DataIntegrityManager DIM = new DataIntegrityManager();
        private Usuario user;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["UrlLog"] = false;
            Session["Intentos"] = 0;
            string integridad = "";
            if (Session["error"] == null)
            {
                List<string> rto = DIM.VerifyDVHs();
                integridad = rto[rto.Count - 1].ToString();
                Session["DVH Result"] = integridad;
                if (integridad == "Corrupted")
                {
                    foreach (string s in rto)
                    {
                        if (s != "Corrupted")
                        {
                            Session["error"] = true;
                            Response.Redirect("error.aspx");
                        }
                    }
                }
                else
                {
                    Start();
                }
            }
            else
            {
                if ((bool)Session["error"] == false)
                {
                    Start();
                }
                else
                {
                    if (Session["Usuario"].ToString().Split('_')[6] == "0")
                    {
                        Response.Redirect("webMaster.aspx");
                    }
                    else
                    {
                        Response.Write("<script>alert('Se necesita intervencion del WebMaster');</script>");
                    }
                }
            }
        }

        private void Start()
        {
            btnCerrar.Visible = false;
            btnLogin.Visible = true;
            if (Session["Usuario"] != null)
            {
                string usuario = Session["Usuario"].ToString();
                user = new Usuario
                {
                    // return ID.ToString() +"_" + Nombre + "_" + Apellido + "_" +
                    // Username + "_" + PassWord + "_" + EMail + "_" + rol.ID.ToString();
                    ID = int.Parse(usuario.Split('_')[0]),
                    Nombre = usuario.Split('_')[1],
                    Apellido = usuario.Split('_')[2],
                    Username = usuario.Split('_')[3],
                    PassWord = usuario.Split('_')[4],
                    EMail = usuario.Split('_')[5],
                    PermisoDeUsuario = new RolesBLL().GetRol(int.Parse(usuario.Split('_')[6]))
                };
                btnCerrar.Visible = true;
                btnLogin.Visible = false;
                lblUsuario.Text = user.Nombre + " " + user.Apellido;
                if (user.PermisoDeUsuario.ID == 0) // webmaster 
                {
                    /*Response.Write("<script>alert('Bienvenido usuario: " + user.Username +
                        ". Usted tiene permisos para: " + user.PermisoDeUsuario.Descripcion + "');</script>");*/
                    btnCrearUsuario.Text = "Consola de WebMaster";
                }
                else if (user.PermisoDeUsuario.ID == 1) // admin
                {
                    /*  Response.Write("<script>alert('Bienvenido usuario: " + user.Username +
                         ". Usted tiene permisos para: " + user.PermisoDeUsuario.Descripcion + "');</script>");*/
                    btnCrearUsuario.Text = "Menu de Administrador";
                }
                else if (user.PermisoDeUsuario.ID == 9) // cliente
                {
                    countlbl.Visible = true;
                    carritobtn.Visible = true;
                    btnCrearUsuario.Visible = false;
                    if (Session["carrito"] != null)
                    {
                        carrito = (List<Producto>)Session["carrito"];
                        countlbl.Text = carrito.Count.ToString() + " productos en el carrito";
                    }
                }
            }
            else
            {

            }
            CargarTablaDeProductos();
        }

        ProductoBLL productoBLL = new ProductoBLL();
        private void CargarTablaDeProductos()
        {
            lblPermiso.Text = "Listado de Productos";
            List<Producto> lista = productoBLL.GetProductos();
            TableRow encabezado = new TableRow();
            encabezado.Cells.Add(new TableCell() { Text = "Codigo" });
            encabezado.Cells.Add(new TableCell() { Text = "Imagen" });
            encabezado.Cells.Add(new TableCell() { Text = "Detalles" });
            encabezado.Cells.Add(new TableCell() { Text = "Precio" });
            if (user != null && user.PermisoDeUsuario.ID == 9)
            {
                encabezado.Cells.Add(new TableCell());
            }
            tablaProductos.Rows.Add(encabezado);
            tablaProductos.Font.Size = 24;
            tablaProductos.CellSpacing = 5;
            foreach (Producto item in lista)
            {
                TableRow fila = new TableRow();
                TableCell id = new TableCell
                {
                    Text = item.Codigo
                };
                TableCell descripcion = new TableCell
                {
                    Text = item.descripcion
                };
                TableCell imagen = new TableCell();
                Image img = new Image();
                TableCell precio = new TableCell
                {
                    Text = "$ " + item.Precio.ToString()
                };
                img.ImageUrl = "~/imgProductos/" + item.imagen;
                img.Height = 150;
                img.Width = 180;
                imagen.Width = 190;
                imagen.VerticalAlign = VerticalAlign.Middle;
                imagen.Controls.Add(img);
                fila.Cells.Add(id);
                fila.Cells.Add(imagen);
                fila.Cells.Add(descripcion);
                fila.Cells.Add(precio);
                TableCell buton = new TableCell();
                Button boton = new Button();
                boton.Text = "Añadir a Carrito";
                buton.HorizontalAlign = HorizontalAlign.Center;
                buton.VerticalAlign = VerticalAlign.Middle;
                buton.Controls.Add(boton);
                boton.Attributes.Add("producto", id.Text);
                buton.Width = 150;
                boton.OnClientClick = "course(); return false;";
                boton.PostBackUrl = "";
                fila.Cells.Add(buton);
                boton.Click += Boton_Click;
                tablaProductos.Rows.Add(fila);
            }
        }

        List<Producto> carrito = new List<Producto>();
        private void Boton_Click(object sender, EventArgs e)
        {
            if (user != null && user.PermisoDeUsuario.ID == 9)
            {
                AttributeCollection attributes = ((Button)sender).Attributes;
                string ProductCode = attributes["producto"].ToString();
                carrito.Add(productoBLL.GetProducto(ProductCode));
                Session["carrito"] = carrito;
                countlbl.Text = carrito.Count.ToString() + " productos en el carrito";
            }
            else
            {
                Response.Write("<script>alert('Usted debe ser un cliente para poder comprar');</script>");
            }
        }

        protected void btnLogin_Click1(object sender, EventArgs e)
        {
            Response.Redirect("logIn.aspx");
        }

        protected void btnCrearUsuario_Click(object sender, EventArgs e)
        {
            if (user == null)
            {
                Session["userClient"]=true;
                Response.Redirect("AltaUsuario.aspx");
            }
            else if (user.PermisoDeUsuario.ID == 0)
            {
                Session["userClient"] = false;
                Response.Redirect("webMaster.aspx");
            }
            else if (user.PermisoDeUsuario.ID == 1)
            {
                Session["userClient"] = false;
                Response.Redirect("administrador.aspx");
            }
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            if (user != null)
            {
                userBLL.LogOut();
                LOG.LogOut(user.Username);
                Response.Write("<script>alert('" + user.Username + ": Sesion cerrada exitosamente');</script>");
                user = null;
                Session["carrito"] = null;
                carrito = null;
                Session["Usuario"] = null;
                Response.Redirect("Home.aspx");
            }
        }

        protected void carritobtn_Click(object sender, ImageClickEventArgs e)
        {
            string message = "";
            foreach (Producto producto in carrito)
            {
                message += producto.ToString();
            }
            Response.Redirect("Carrito.aspx");
        }
    }
}