﻿using BLL;
using BussinesEntity;
using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RunnerShop
{
    public partial class bitacora : System.Web.UI.Page
    {
        private readonly Bitacora LOG = new Bitacora();
        Usuario user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Usuario"] != null && Session["UrlLog"].ToString() == "True")
            {
                string usuario = Session["Usuario"].ToString();
                user = new Usuario
                {
                    // return ID.ToString() +"_" + Nombre + "_" + Apellido + "_" +
                    // Username + "_" + PassWord + "_" + EMail + "_" + rol.ID.ToString();
                    ID = int.Parse(usuario.Split('_')[0]),
                    Nombre = usuario.Split('_')[1],
                    Apellido = usuario.Split('_')[2],
                    Username = usuario.Split('_')[3],
                    PassWord = usuario.Split('_')[4],
                    EMail = usuario.Split('_')[5],
                    PermisoDeUsuario = new RolesBLL().GetRol(int.Parse(usuario.Split('_')[6]))
                };
                lblUsuario.Text = user.Nombre + " " + user.Apellido;
                if (user.PermisoDeUsuario.ID == 0)
                {
                    List<string> lista = LOG.GetLogs();
                    foreach (var item in lista)
                    {
                        TableRow fila = new TableRow();
                        TableCell id = new TableCell();
                        TableCell fecha = new TableCell();
                        TableCell mensaje = new TableCell();

                        string[] line = item.Split(':');
                        id.Text = line[0];
                        fecha.Text = line[1] + ":" + line[2] + ":" + line[3];
                        mensaje.Text = line[4].Substring(1);

                        fila.Cells.Add(id);
                        fila.Cells.Add(fecha);
                        fila.Cells.Add(mensaje);
                        tablaBitacora.Rows.Add(fila);
                    }
                }
                else
                {
                    Response.Write("<script>alert('Usted no puede hacer esto!');</script>");
                    Session["UrlLog"] = false;
                    Response.Redirect("Home.aspx");
                }
            }
            else
            {
                Response.Redirect("Home.aspx");
            }
        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            Session["UrlLog"] = false;
            Response.Redirect("webMaster.aspx");
        }
    }
}