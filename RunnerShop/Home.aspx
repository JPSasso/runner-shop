﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="RunnerShop.Home"  enableSessionState="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>RunnerShop</title>    
    <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin=""/>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet"/>
    <link href="/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            margin-left: 0px;
            margin-right: 16px;
            margin-top: 3px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <table style="width: 100%;" class="center">
            <tr>
                <td class="aLeft">
                    <asp:Image ID="logo1" runat="server"  ImageUrl="~/imagenes/Logo.png" Height="40" ImageAlign="Middle" />
                </td>
                <td class="aRight">
                    <asp:Label ID="countlbl" CssClass="lblUsuario" runat="server" Text="cuenta" Visible="false" ></asp:Label> &nbsp;&nbsp;&nbsp;                   
                    <asp:ImageButton ID="carritobtn" ImageUrl= "~/imagenes/cart-solid-24.png" BorderWidth="1px" CssClass="auto-style1" 
                        runat="server" Text=" " Visible="False" Width="25px" OnClick="carritobtn_Click"  />

                    <asp:Label ID="lblUsuario" CssClass="lblUsuario" runat="server" Text="usuario" ></asp:Label> &nbsp;
                    <asp:Button ID="btnLogin" CssClass="button" runat="server" Text="Iniciar Sesion" Width="100px" OnClick="btnLogin_Click1" />
                    <asp:Button ID="btnCrearUsuario" CssClass="button" runat="server" Text="Registrarse" OnClick="btnCrearUsuario_Click"/>
                    <asp:Button ID="btnCerrar" CssClass="button" runat="server" Text="Cerrar Sesion" Visible="False" Width="100px" OnClick="btnCerrar_Click" />
                </td>
            </tr>
        </table>        
        <br />
        <asp:Label runat="server" ID="lblPermiso" text="Listado de Productos" Font-Bold="True" Font-Size="Medium" CssClass="aCenter"></asp:Label>
        <br />
        <asp:Table ID="tablaProductos" class="tablaProductos" runat="server"></asp:Table>
        </div>
    </form>
        </body>
</html>
