﻿using BLL;
using BussinesEntity;
using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RunnerShop
{
    public partial class Carrito : System.Web.UI.Page
    {
        Usuario user;
        ProductoBLL productoBLL = new ProductoBLL();
        List<Producto> carrito = new List<Producto>();

        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = Session["Usuario"].ToString();
            user = new Usuario
            {
                // return ID.ToString() +"_" + Nombre + "_" + Apellido + "_" +
                // Username + "_" + PassWord + "_" + EMail + "_" + rol.ID.ToString();
                ID = int.Parse(usuario.Split('_')[0]),
                Nombre = usuario.Split('_')[1],
                Apellido = usuario.Split('_')[2],
                Username = usuario.Split('_')[3],
                PassWord = usuario.Split('_')[4],
                EMail = usuario.Split('_')[5],
                PermisoDeUsuario = new RolesBLL().GetRol(int.Parse(usuario.Split('_')[6]))
            };
            btnCerrar.Visible = true;
            lblUsuario.Text = user.Nombre + " " + user.Apellido;
            if (Session["carrito"] != null)
            {
                carrito = (List<Producto>)Session["carrito"];
                float total = 0f;
                foreach (Producto producto in carrito)
                {
                    total += producto.Precio;
                }
                TextBox1.Text += "\t$ " + total.ToString();
            }
            CargarTablaDeProductos();
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }
        private void CargarTablaDeProductos()
        {
            List<Producto> lista = carrito;
            TableRow encabezado = new TableRow();
            encabezado.Cells.Add(new TableCell() { Text = "Codigo" });
            encabezado.Cells.Add(new TableCell() { Text = "Imagen" });
            encabezado.Cells.Add(new TableCell() { Text = "Detalles" });
            encabezado.Cells.Add(new TableCell() { Text = "Precio" });
            if (user != null && user.PermisoDeUsuario.ID == 9)
            {
                encabezado.Cells.Add(new TableCell());
            }
            tablaProductos.Rows.Add(encabezado);
            tablaProductos.Font.Size = 24;
            tablaProductos.CellSpacing = 5;
            foreach (Producto item in lista)
            {
                TableRow fila = new TableRow();
                TableCell id = new TableCell
                {
                    Text = item.Codigo
                };

                TableCell descripcion = new TableCell
                {
                    Text = item.descripcion
                };

                TableCell imagen = new TableCell();
                Image img = new Image();

                TableCell precio = new TableCell
                {
                    Text = "$ " + item.Precio.ToString()
                };

                img.ImageUrl = "~/imgProductos/" + item.imagen;
                img.Height = 150;
                img.Width = 180;
                imagen.Width = 190;
                imagen.VerticalAlign = VerticalAlign.Middle;
                imagen.Controls.Add(img);

                fila.Cells.Add(id);
                fila.Cells.Add(imagen);
                fila.Cells.Add(descripcion);
                fila.Cells.Add(precio);
                tablaProductos.Rows.Add(fila);
            }
        }
        Bitacora Log = new Bitacora();
        protected void bntBuy_Click(object sender, EventArgs e)
        {
            Log.CustomLog(Bitacora.TipoMensaje.Error, "El cliente " + user.Username + " quizo realizar una compra");
            Response.Write("<script>alert('Compra disponible en proximas actualizaciones');</script>");
        }
    }
}