﻿using BLL;
using BussinesEntity;
using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RunnerShop
{
    public partial class AltaUsuario : System.Web.UI.Page
    {
        UserBLL usuarioBLL = new UserBLL();
        Usuario user;
        Bitacora Log = new Bitacora();
        DataIntegrityManager DIM = new DataIntegrityManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if((bool)Session["userClient"] == true)
            {
                DropDownList1.Visible = false;
            }
            else
            {
                DropDownList1.Visible = true;
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            if (Session["Usuario"] != null)
            {
                string usuario = Session["Usuario"].ToString();
                user = new Usuario
                {
                    // return ID.ToString() +"_" + Nombre + "_" + Apellido + "_" +
                    // Username + "_" + PassWord + "_" + EMail + "_" + rol.ID.ToString();
                    ID = int.Parse(usuario.Split('_')[0]),
                    Nombre = usuario.Split('_')[1],
                    Apellido = usuario.Split('_')[2],
                    Username = usuario.Split('_')[3],
                    PassWord = usuario.Split('_')[4],
                    EMail = usuario.Split('_')[5],
                    PermisoDeUsuario = new RolesBLL().GetRol(int.Parse(usuario.Split('_')[6]))
                };
                if (user.PermisoDeUsuario.ID == 0)
                {
                    Response.Redirect("webMaster.aspx");
                }
                else
                {
                    Response.Redirect("Home.aspx");
                }
            }
            else
            {
                Response.Redirect("Home.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Usuario user = new Usuario
            {
                ID = int.Parse(txtId.Text),
                Nombre = txtNombre.Text,
                Apellido = txtApellido.Text,
                EMail = txtCorreo.Text,
                Username = txtUsuario.Text,
                PassWord = txtPass.Text,
                Bloqueado = false
            };
            user.PermisoDeUsuario = (bool)Session["userClient"] == true ? new RolesBLL().GetRol(9) : new RolesBLL().GetRol(DropDownList1.SelectedItem.ToString());
            int rto = usuarioBLL.altaUsuario(user);
            if (rto >= -1)
            {
                Response.Write("<script>Alert('Se dio de alta al usuario');</script>");
                Log.AltaUsuario(user.Username);
            }
            DIM.CreateSnapShotFile();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

        }
    }
}