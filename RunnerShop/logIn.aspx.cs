﻿using BLL;
using BussinesEntity;
using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RunnerShop
{
    public partial class logIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnEnviar.Focus();
        }
        private int Intentos;
        private string username;
        private readonly UserBLL userBLL = new UserBLL();
        private readonly Bitacora Log = new Bitacora();
        private Usuario user;

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            user = new Usuario();
            if (int.Parse(Session["Intentos"].ToString()) < 2)
            {
                if (user.Bloqueado)
                {
                    Response.Write("<script>alert('Demasiados intentos fallidos, Usuario bloqueado. Contacte al WebMaster para restaurar su usuario');</script>");
                }
                else
                {
                    username = txtUsuario.Text;
                    switch (userBLL.LogIn(txtUsuario.Text, txtPAss.Text))
                    {
                        case 1:
                            user = userBLL.GetUsuario(txtUsuario.Text);
                            Session["Usuario"] = user;
                            Log.LogIn(user.Username);
                            Response.Redirect("Home.aspx");
                            break;
                        case -2:
                            txtPAss.Text = string.Empty;
                            Intentos = int.Parse(Session["Intentos"].ToString());
                            Intentos++;
                            Session["Intentos"] = Intentos;
                            Response.Write("<script>alert('Contraseña invalida');</script>");
                            break;
                        case -3:
                            txtPAss.Text = string.Empty;
                            txtUsuario.Text = string.Empty;
                            Intentos = int.Parse(Session["Intentos"].ToString());
                            Intentos++;
                            Session["Intentos"] = Intentos;
                            Response.Write("<script>alert('Usuario invalido');</script>");
                            break;
                    }
                }
            }
            else
            {
                userBLL.BloquearUsuario(txtUsuario.Text);
                user.Bloqueado = true;
                Log.CustomLog(Bitacora.TipoMensaje.Advertencia,
                    "Usuario: " + txtUsuario.Text + " bloqueado por demasiados intentos fallidos de iniciar sesion");
                Response.Write("<script>alert('Demasiados intentos fallidos, Usuario bloqueado');</script>");
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }

        protected void btnRecuperar_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Recupero de contraseña por email aun no disponible. " +
                "Para recuperar su contraseña deberá contactarse con el WebMaster');</script>");
        }
    }
}