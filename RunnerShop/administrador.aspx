﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="administrador.aspx.cs" Inherits="RunnerShop.administrador"enableSessionState="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>RunnerShop</title>
    <link href="/estilos/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;" class="center">
                <tr>
                    <td class="aLeft">
                        <asp:Image ID="logo1" runat="server" ImageUrl="~/imagenes/logo.png" Height="40" ImageAlign="Middle" />
                    </td>
                    <td class="aRight">
                        <asp:Label ID="lblUsuario" runat="server" Text="usuario"></asp:Label>
                        &nbsp;
                    <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="100px"  CssClass="button" OnClick="btnCerrar_Click1" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:Label runat="server" ID="lblPermiso" Text="Perfil: Administrador" Font-Bold="True" Font-Size="Medium" CssClass="aCenter"></asp:Label>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="Alta de Productos" OnClick="Button1_Click" CssClass="button" />

        </div>
    </form>
</body>
</html>
