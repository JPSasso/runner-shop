﻿using Seguridad_Servicios;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RunnerShop
{
    public partial class backUp : System.Web.UI.Page
    {
        DataIntegrityManager DGM = new DataIntegrityManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Session["backup"] = "RunnerShop_" + DateTime.Now.ToString("yyMMdd-HHmmss");
                new Bitacora().BackUp();
            if (DGM.BackUp(Session["backup"].ToString()) >= -1)
            {
                lblmensaje.Text = "Copia de seguridad exitosa";
            }
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            Response.Redirect("webMaster.aspx");
        }

        protected void btnRestaurar_Click1(object sender, EventArgs e)
        {
            try
            {
                if (FileUpload1.FileName != "")
                {
                    Session["backup"] = FileUpload1.FileName;
                }
                else
                {
                    lblmensaje.Text = "Seleccione un archivo para restaurar";
                }
                if (DGM.Restore(Session["backup"].ToString()) >= -1)
                {
                    lblmensaje.Text = "Sistema Restaurado!";
                    new Bitacora().Restore();
                }
            }
            catch { }
        }
    }
}