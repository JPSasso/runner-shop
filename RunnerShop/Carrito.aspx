﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Carrito.aspx.cs" Inherits="RunnerShop.Carrito" enableSessionState="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>RunnerShop-Carrito</title>
<link href="/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            margin-left: 635px;
        }
        .auto-style2 {
            margin-left: 630px;
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;" class="center">
            <tr>
                <td class="aLeft">
                    <asp:Image ID="logo1" runat="server"  ImageUrl="~/imagenes/logo.png" Height="40" ImageAlign="Middle" />
                </td>
                <td class="aRight">
                    <asp:Label ID="lblUsuario" runat="server" Text="usuario"></asp:Label>&nbsp;
                    <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="100px"  CssClass="button" OnClick="btnCerrar_Click"/>
                </td>
            </tr>
        </table>        
        <br />   
        <asp:Label runat="server" ID="lblPermiso" text="Tus productos en el carrito" Font-Bold="True" Font-Size="Medium" CssClass="aCenter"></asp:Label>
        <br />
        <asp:Table ID="tablaProductos" class="tablaProductos" runat="server"></asp:Table>
        <br /> <br />
        &nbsp;             
        <asp:TextBox ID="TextBox1" runat="server" Text="Monto total" BackColor="#BFC9CA" BorderColor="#BFC9CA" 
            BorderStyle="None" CssClass="auto-style2" Width="184px">Monto total</asp:TextBox>
        <br /> <br /> 
        <asp:Button ID="bntBuy" runat="server" Text="Finalice su compra" 
            OnClientClick="course(); return false;" CssClass="auto-style1" OnClick="bntBuy_Click"/>
        <br /> <br /></div>
    </form>
</body>
</html>
