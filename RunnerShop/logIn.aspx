﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="logIn.aspx.cs" Inherits="RunnerShop.logIn" enableSessionState="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>RunnerShop</title>
    <link href="/estilos/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <table style="width: 100%;" class="center">
            <tr>
                <td class="aLeft">
                    <asp:Image ID="logo1" runat="server" ImageUrl="~/imagenes/Logo.png" Height="40" ImageAlign="Middle" />
  </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <table style="width:620px;" class="center">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Usuario</td>
                <td>
                    <asp:TextBox ID="txtUsuario" runat="server" Width="300px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsuario" ErrorMessage="Campo Requerido" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Contraseña</td>
                <td>
                    <asp:TextBox ID="txtPAss" runat="server" TextMode="Password" Width="300px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPAss" ErrorMessage="Campo Requerido" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1"></td>
                <td class="auto-style1">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="btnEnviar" runat="server" Text="Enviar" Width="100px" OnClick="btnEnviar_Click" CssClass="button" />&nbsp;
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="100px" CausesValidation="False" OnClick="btnCancelar_Click" CssClass="button" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnRecuperar" runat="server" Text="Recuperar Contraseña" BorderStyle="None" CausesValidation="False" OnClick="btnRecuperar_Click" CssClass="button" />
                    </td>
                <td>
                    <asp:Label ID="lblNueva" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>    
    </div>
    </form>
</body>
</html>
