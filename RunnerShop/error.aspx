﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="RunnerShop.error"enableSessionState="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>RunnerShop-ERROR</title>
<link href="/estilos/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;" class="center">
            <tr>
                <td class="aLeft">
                    <asp:Image ID="logo1" runat="server"  ImageUrl="~/imagenes/logo.png" Height="40" ImageAlign="Middle" />
                </td>
                <td class="aRight">
                    <asp:Label ID="lblUsuario" runat="server" Text="usuario"></asp:Label>&nbsp;
                    <asp:Button ID="btnLogin" runat="server" Text="Login" Width="100px" OnClick="btnLogin_Click" CssClass="button"/>
                    <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="100px" OnClick="btnCerrar_Click" CssClass="button"/>
                </td>
            </tr>
        </table>        
        <br />        
        <br />
        <table style="width: 650px;" class="center">
            <tr>
                <td>                    
                    <asp:Label ID="Label1" runat="server" Text="Revise integridad de la bases de datos" ForeColor="Red" Font-Size="14"></asp:Label>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Image ID="Image1" runat="server"  ImageUrl="~/imagenes/mantenimiento.gif" ImageAlign="Middle" Height="285px" Width="655px" />
                </td>
            </tr>
        </table>
        </div>
    </form>
</body>
</html>
