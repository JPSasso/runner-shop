﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="backUp.aspx.cs" Inherits="RunnerShop.backUp"enableSessionState="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>RunnerShop</title>
<link href="/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            width: 375px;
            height: 30px;
        }
        .auto-style2 {
            width: 186px;
            height: 30px;
        }
        .auto-style3 {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            margin-left: 0px;
            margin-top: 13px;
        }
        .auto-style4 {
            width: 186px;
            height: 27px;
        }
        .auto-style5 {
            width: 375px;
            height: 27px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;" class="center">
                <tr>
                    <td class="aLeft">
                        <asp:Image ID="logo1" runat="server" ImageUrl="~/imagenes/logo.png" Height="40" ImageAlign="Middle" />
                    </td>
                    <td class="aRight">
                        <asp:Label ID="lblUsuario" runat="server" Text="usuario"></asp:Label>
                        &nbsp;
                    <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="100px" CssClass="button" OnClick="btnCerrar_Click" />
                    </td>
                </tr>
            </table>

            <br />
            <asp:Label runat="server" ID="lblPermiso" Text="Perfil: Webmaster" Font-Bold="True" Font-Size="Medium" CssClass="aCenter"></asp:Label>
            <br />
            <br />
            <asp:Label runat="server" ID="Label2" Text="Copia de Seguridad y Restauración" Font-Bold="True" Font-Size="Medium" CssClass="aCenter"></asp:Label>
            <br />
            <br />
            <br />
            <table style="width: 48%; height: 138px;" class="center">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="Label5" runat="server" Text="Hacer Copia de seguridad"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:Button ID="btnBack" runat="server" Text="Ejecutar" OnClick="btnBack_Click" CssClass="button" OnClientClick="course(); return false;" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label3" runat="server" Text="Restaurar Copia de seguridad"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:Button ID="btnRestaurar" runat="server" Text="Ejecutar"  CssClass="button" OnClick="btnRestaurar_Click1" OnClientClick="course(); return false;"/>
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="auto-style3" Width="480px" />
                    </td>
                   <tr>
                        <td colspan="2">
                            <asp:Label ID="lblmensaje" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
